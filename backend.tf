terraform {
  backend "s3" {
    bucket = "bucket_name"
    key    = "dev/eks/main/terraform.tfstate"
    region = "us-east-1"
  }
}
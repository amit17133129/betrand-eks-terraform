module "eks" {
    source = "./modules"
    cidr_vpc                     = var.cidr_vpc
    cidr_subnet1                 = var.cidr_subnet1
    cidr_subnet2                 = var.cidr_subnet2
    availability_zone            = var.availability_zone
    environment_tag              = var.environment_tag
    security_group_name          = var.security_group_name
    route_table_name             = var.route_table_name
    ig_name                      = var.ig_name
    iam_role_name                = var.iam_role_name
    eks_node_group_name          = var.eks_node_group_name
    eks_cluster_name             = var.eks_cluster_name
    iam_instance_profile_name    = var.iam_instance_profile_name
    node_group_name              = var.node_group_name
}
resource "aws_route_table" "r" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "${var.environment_tag}-${var.route_table_name}"
  }
}

resource "aws_route_table_association" "public1" {
  subnet_id = "${aws_subnet.subnet_public_Lab1.id}"  
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_route_table_association" "public2" {
  subnet_id = "${aws_subnet.subnet_public_Lab2.id}"  
  route_table_id = "${aws_route_table.r.id}"
}


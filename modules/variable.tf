variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "192.168.0.0/16"
}



variable "cidr_subnet1" {
  description = "CIDR block for the subnet"
  default = "192.168.1.0/24"
}
variable "cidr_subnet2" {
  description = "CIDR block for the subnet"
  default = "192.168.2.0/24"
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  default = "ap-south-1"
}
variable "environment_tag" {
  description = "Environment tag"
  default = "Production"

}


variable "sg_protocols_ingress" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
  }))

  default = [
    {
      from_port = 80
      to_port = 80
      protocol = "tcp"
    },
    {
      from_port = 22
      to_port = 22
      protocol = "tcp"
    }
  ]
}

variable "sg_protocols_egress" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
  }))

  default = [
    {
      from_port = 0
      to_port = 0
      protocol = "-1"
    }
  ]
}

variable "security_group_name" {
  description = "security group name"
}

variable "route_table_name" {
  description = "route table name"
}

variable "ig_name" {
  description = "internet gateway name"
}
variable "iam_role_name" {
  description = "eks iam role name"
}


variable "eks_node_group_name" {
  description = "eks node group name"
}

variable "eks_cluster_name" {
  description = "eks node group name"
}

variable "iam_instance_profile_name" {
  description = "eks node group name"
}

variable "node_group_name" {
  description = "eks node group name"
}
resource "aws_vpc" "vpc" {
  cidr_block = "${var.cidr_vpc}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags ={
    Name= "${var.environment_tag}-vpc"
  }
}

resource "aws_subnet" "subnet_public_Lab1" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.cidr_subnet1}"
  map_public_ip_on_launch = "true"
  availability_zone = "us-east-1a"
  tags ={
    Name= "${var.environment_tag}-subnet-1"
  }

}
resource "aws_subnet" "subnet_public_Lab2" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.cidr_subnet2}"
  map_public_ip_on_launch = "true"
  availability_zone = "us-east-1b"
  tags ={
    Name= "${var.environment_tag}-subnet2"
  }

}

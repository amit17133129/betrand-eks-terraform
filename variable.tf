variable "cidr_vpc" {
  description = "CIDR block for the VPC"
}
variable "cidr_subnet1" {
  description = "CIDR block for the subnet"
  
}
variable "cidr_subnet2" {
  description = "CIDR block for the subnet"
  
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  
}
variable "environment_tag" {
  description = "Environment tag"

}

variable "security_group_name" {
  description = "security group name"
}


variable "route_table_name" {
  description = "route table name"
}

variable "ig_name" {
  description = "internet gateway name"
}
variable "iam_role_name" {
  description = "eks iam role name"
}


variable "eks_node_group_name" {
  description = "eks node group name"
}

variable "eks_cluster_name" {
  description = "eks node group name"
}

variable "iam_instance_profile_name" {
  description = "eks node group name"
}

variable "node_group_name" {
  description = "eks node group name"
}